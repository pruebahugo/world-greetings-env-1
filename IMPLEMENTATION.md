
### Implementation Details

This documentation contains implementation details for those wishing to modify or reuse this project. It attempts to explain implementation decisions that might be creating value or adhering to constraints that are non-obvious in the code. In this regard it attempts to articulate where to take care in changing the implementation.

[TOC]

### Designed for Training AND As A Production Scaffold

Some conventions in this project are designed to ease multi-participant training - while still trying not to detract from reuse as a scaffold for real world projects.

This includes:
- The assumption that a shared Kubernetes cluster may be used for the entire class - unique projects and kubernetes namespaces are needed for each student.
- The assumption that all participants may work within a common root GitLab group - using either per-participant subgroups or per-participant project unique prepended names.
- The assumption that GitLab.com SaaS or any self-managed instance might be used - by importing the projects into the instance.
- The assumption that any kind of Kubernetes cluster supported by GitLab may be used - these examples are agnostic to the specific K8s cluster in use.

These conventions also do not detract from use in learning where none of the above are shared (e.g. self-paced learning).

#### Automatic Uniqueness by Copying Projects

The projects are designed to be copied and minimally altered to create a unique copy for each student, but still facilitate using a single target cluster and be within a single GitLab group heirarchy.

The Application Project is fully abstracted when copying by taking on the project slug as the service name. If it is copied into a unique subgroup per-student, it does even need to be renamed. This works in tandem with the Kubernetes namespace uniqueness details in the next section.

#### Unique K8s Namespace and Non-Unique Service Names

The service name of the Application does not need to change even if everyone is deploying the same projects to the same cluster because the namespace in each Environment Deployment project is set to: ${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${CI_ENVIRONMENT_NAME}.

However, if copying the hello-world-service project to demonstrate multi-service projects - then, as per usual, the service name for the second service would need to be unique in the package manifests.  Copying the hellow-world-service App Project does cause full scaffolding of that project by using the project url slug as the service name.

#### GitLab K8s Agent Setup for Classroom Shared Cluster

Placeholder

### Experimenting
The variables NEXTVERSIONTOUSE and CD_STRATEGY can be set when calling a pipeline in order to experiment. These variables should generally not be used during pipeline execution once you've productionized your approach. Instead, their defaults should be set in code to how you want the project to behave. The one exception is the Version Strategy "Manual Trigger without MRs" in which case changing the variable NEXTVERSIONTOUSE and running the pipeline is the primary trigger for updating to a new version.

### Internal Child Pipeline Requirement
An internal child pipeline is required because the version change determination is dynamically determined in the version checking job and an .ENV file is used to set it at the pipeline level. The child pipeline allows for this value to be used to determine what child pipeline jobs should be scheduled by pipeline preprocessing. A unified pipeline cannot use a value from a job to determine what jobs should run because this determination happens as a preprocessing step before any job starts.

### rules:changes Constraints
Instead of `rules:changes` this project uses `$CI_PIPELINE_SOURCE == "push"` to run when any files have changed. This is because `rules:changes` is always true when a pipeline runs for a reason other than a file change and so can actually cause the child pipeline jobs to run when not necessary.

### CD_STRATEGY Variable Constraints
The bash variable expansion `${CD_STRATEGY##*-}` is used to trim the end of the value of CD_STRATEGY to be used as the "environment". This implies some restrictions on modifications:
1. That the data values always end in a valid environment used in the kubernetes manifest name and a couple other places
2. That trying to process this variable expansion in containers shells like busybox 'sh' may fail (they don't support all bash variable expansions) - this can be worked around by adding sed commands, etc. This is one of the reasons for preferring the bash container outlined below. 

### Skopeo Container Management Utility
[Skopeo](https://github.com/containers/skopeo) allows container operations on registries without having docker installed. This includes remote metadata retrieval and moving containers between repositories. It can be a great way to sync repositories and promote containers between pre-production and production registries. Google publishes a simpler utility called [Crane](https://github.com/google/go-containerregistry/tree/main/cmd/crane) - it is simpler, but it is not published in many package managers (including alpine apk that the bash container is based on) and using it's container is restrictive due to busybox sh. By contrast it installs so quickly on the bash container that the install time is a wash compared to loading another container.

### Unique Utilities Utilized

- **Advanced Container Management in CI** - Both skopeo and crane container management utilities are utilized to both read image metadata and write tags to repositories without utilizing docker to do so.
- **Non-Privilege Mode Container Builds** - Utilizes custom build stage with Kaniko.

### Bash Container With Runtime Installations (Respect for Human and Machine Resources)

You will notice that this project has a propensity to use the bash container and do runtime installs on it rather than use utility containers.

GitLab scripts run inside third party containers - you must manually override their entrypoint to get a shell. Frequently these containers have a minimalized shell environment (e.g. BusyBox sh) and do not have a package manager installed. The Skopeo and Crane containers were examples experienced in this project. When these minimalized containers are leveraged directly it can cause net negative impacts on human productivity and resource consumption such as:

- excessive fragmentation of CI/CD script logic across jobs - especially when some of the script code needs to be shared among multiple jobs across complex pipelines (Code complexity tax)
- early optimization and overhead of creating pipeline specific containers that aren't very different from the container they are based on (over modularization tax)
- verbose shell code for relatively simple operations in a full shell - sme notable one being all the variable expansion parsing and if statement comparisons available in bash 5. (code complexity + prototyping taxes)
- lack of basic automation utilities like wget (dependency checking and resolution tax)
- excessive use of artifacts and/or runner cache to do bits of processing in various containers which results in storage consumption and job startup delays
- container retrieval and instantiation delays - especially on shared runners where security concerns require the repeated us of the same container still pulls the image from the source registry for each job.

By using the bash container and doing a few reasonable quick installs we do not suffer from these limitations and in many cases may have a faseter pipeline. If the number of installs for a specific container go over a threshold for a reasonable wait time, we can then break out a Dockerfile for those installs - but this avoid early optimization of building a pipeline specific container until then.

**De-Automation** (*n*): What results when an alleged automation effort over-focuses on machine resources optimization to the point of leading to a net deoptimization of scaled human activities - like impacting a 'bit' of time for every developer each day. Just do the numbers on a 15 minute (1%) impact per week across the total benefits of any group of 250 or more people. #happensmoreoftenthanyouthink ;)
