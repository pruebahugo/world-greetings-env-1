# GitLab Agent for Kubernetes demo: Environment Configuration and Deployment Projects

[TOC]


### Many Apps to Many Environments Are Also Possible

While this model shows one service from one Application Project being deployed by one Environment Deployment Project - it can also be a great starting point for modelling a multi-service application that consumes multiple Application Project outputs which may have multiple Environment Deployment Projects for various purposes.

Some applications may have multiple or many production environments to accomodate patterns such as:

- **Dedicated Tenancy** - one production per customer - sometimes with their own staging environment and sometimes with customer control over new version deployment.
- **Geographical Tenancy for Data Sovereignty** - Data must reside in a specific country.
- **Multi-GEO Tenancy for Performance** - global performance.
- **Multi-Cloud Tenancy** - Avoid lock in, meet customer requirements.
- **Seperation of Ops Governance** - Strong separation of security, Ops duties and/or release cycles between Dev and Ops and/or between multiple Ops teams. For example Gov and Commercial deployments of the same software.

### CD Strategies

1. To do continuous delivery to staging set CD_STRATEGY to 'deliver-to-staging' - the manifest file is collected as an artifact and manual approval will be required to proceed to production.
2. To do continuous deployment set CD_STRATEGY to 'deploy-to-production' - the manifest file is collected as an artifact, production deployment happens automatically. This assumes that you trust the artifact generating repository testing well enough to do continuous deployment to production. Whether you trust it has a lot to do with the record of quality and any seperation of duties concerns such as deploying a commercial software release to a GovCloud environment or when customer tenants are in control of when they take changes.

### Loose Project Coupling

**Loose Coupling** means that there are the fewest dependencies in terms of shared security, shared configuration and shared pipeline execution between the project building the application (aka Application Project) and the project(s) consuming the build artifacts (aka Environment Deployment Project(s)). It facilitates high scale of Environment Deployment projects against a given Application Project. It also facilitates seperation of duties between Dev and Ops and/or multiple Ops teams. A Loose Coupling approach is supported by the following 12 Factor App principles: "[I. Codebase](https://12factor.net/codebase)", "[III. Config](https://12factor.net/config)" and "[X. Dev/prod party](https://12factor.net/dev-prod-parity)"

For instance, the only security requirements between an Application Project and an Environment Deployment Project are:
- The Environment Deployment project needs a "Read Registry" token to the Application Project - it can be created as an Access Token at a group level that scopes in all Environment Deployment projects that utilize that need the Application Project container. This is read only access - so relatively safe to share the token in a CI CD variable.
- The Environment Deployment project needs to be able to commit back to itself. This token can be published at the project level to keep the scope it is shared in to the least possible.

### Version Updating Strategies (with 12factor App Support)

#### 1. Automatic via Monitoring Container Registry (Loose Coupling)
To enable auto-updating when a new image version appears either do a scheduled pipeline or a pipeline subscription (runs shortly after parent pipeline) to the image creating repository. NEXTVERSIONTOUSE must be set to 'read-from-registry'.

#### 2. Manual Trigger without MRs (Loose Coupling)
To manually update versions without MRs, during a manual pipeline set NEXTVERSIONTOUSE to a valid image tag for the monitored registry. Ensure there are no pipeline subscriptions or schedules which will auto-update over top of your forced version.

#### 3. Manual Trigger with MRs (Loose Coupling)
To use Merge Requests to change versions, do an MR that creates a file NEXTVERSIONTOUSE that contains one line with the next version on it and set NEXTVERSIONTOUSE to 'read-from-file'.

#### 4. Automatic via Calling Child Pipelines From App Project (Strong Coupling) 
To use a parent pipeline trigger, link the trigger to update-manifests.gitlab-ci.yml and be sure to pass the following variables: CD_STRATEGY, SERVICE_NAME, IMAGE_NAME_TO_MONITOR,  VERSIONUPDATE, NEXTVERSION.

  NOTE: This is less scalable and more tightly coupled so should only be done where one or more of the following is desirable and/or acceptable:

  1. there is only one or very few subscribing Environment Deployment projects per app project, 
  2. deployment triggers are desired to be immediate upon new container building,
  3. deployment status needs to be reflected in the app project. Be mindful of whether you want the app project pipeline to fail if a deployment fails.

### Pipeline "Passed" Status
To be the most efficient the only stage that must run is the version check. When nothing has changed the deploy job to call the child pipeline will fail. It is marked to be allowed to fail which results in a pipeline status of "Passed". This design allows the most efficient number of pipelines but child pipeline jobs fail because they generally don't expect to have nothing to schedule. The deploy job itself cannot be conditioned on the value of VERSIONUPDATE because job inclusion is evaluated before the pipeline starts.

### GitLab Environments: Capabilities and Limitations In This GitOps Deploy Project
The following features of GitLab Environments work as designed:
- Environments View
- Protected Environments

The following features of GitLab Environments do not work fully as designed:
- Since the GitLab Kubernetes Agent is in a pull mode for this example, the status in the Environments console really means "The Environment Manifest Was Updated" (or is being updated). The final status in the actual cluster is not being tracked.
- Pipelines run on a specific commit, since GitOps requires a new commit DURING the pipeline, the commit tracked in environments is always 1 behind (it is the one that made the new manifest commmit).
- Environments cannot be stopped.

### Implementation Details and Experimenting
The information in [IMPLEMENTATION.md](IMPLEMENTATION.md) may be useful to those wishing to customize the code for their own purposes. It documents some of the hard lessons that are not easy to learn by direct reading of the code.

### Enabled for Classroom Training
You can read about how this project and its companion project are constructed to work in a multi-participant training environment here: [Designed for Training AND As A Production Scaffold](IMPLEMENTATION.md#designed-for-training-and-as-a-production-scaffold)
